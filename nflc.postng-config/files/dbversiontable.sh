#!/usr/bin/env bash

# get args postngdbname
DBNAME=$1

CHECKTABLE="SELECT 'dbversion' FROM information_schema.tables WHERE  table_catalog = '$DBNAME' AND table_name = 'dbversion';"

# check if db exists
if psql -d "$DBNAME" -lqt | cut -d \| -f 1 | grep -w "$DBNAME"; then
    # database exists

    # check if table 'dbversion' exists
    if psql -d "$DBNAME" -c "$CHECKTABLE" | grep -w 'dbversion'; then
        # otherwise, we assume it's already been created; return exit code 0
        exit 0
    else
        # if table not exists, then create and return exit code 64
        psql -d $DBNAME <<EOF
\x
CREATE TABLE dbversion (
    "version" text,
    "notes" text,
    "added" text,
    UNIQUE ("version")
);
EOF
        exit 64
    fi

else
    # no database; return a general error code
    exit 1
fi

